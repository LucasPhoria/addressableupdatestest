using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneLoader : MonoBehaviour
{

	public AssetReference sceneA;
	public AssetReference sceneB;
	public AssetReference sceneC;

	public Button loadAButton;
	public Button loadBButton;
	public Button loadCButton;

//	public Addressables.MergeMode mergeMode = Addressables.MergeMode.Intersection;
	
//	public List<AssetLabelReference> labels = new List<AssetLabelReference>();

//	public async void Start()
//	{
//		
//		await Addressables.InitializeAsync().Task;
//
//		var locations = await Addressables.LoadResourceLocationsAsync(keys: labels,
//		                                                              mode: mergeMode).Task;
//
//		foreach (var l in locations)
//		{
//			var output = "Location\n\n";
//
//			output += $"InternalId\t\t[{l.InternalId}]\n";
//			output += $"ProviderId\t\t[{l.ProviderId}]\n";
//			output += $"PrimaryKey\t\t[{l.PrimaryKey}]\n";
//
//			output += "\n";
//			
//			Debug.Log(output);
//		}
//		
//		var bytes = await Addressables.GetDownloadSizeAsync(locations).Task;
//
//		Debug.Log($"Download size\n[{bytes}]");
//
//	}
	
	public async void LoadA()
	{
		if (sceneA.IsValid())
		{
			await sceneA.UnLoadScene().Task;

			loadAButton.GetComponentInChildren<Text>().text = "Load Scene A";
		}
		else
		{
			await sceneA.LoadSceneAsync(loadMode: LoadSceneMode.Additive,
			                            activateOnLoad: true).Task;

			loadAButton.GetComponentInChildren<Text>().text = "Unload Scene A";
		}
	}
	
	public async void LoadB()
	{
		if (sceneB.IsValid())
		{
			await sceneB.UnLoadScene().Task;

			loadBButton.GetComponentInChildren<Text>().text = "Load Scene B";
		}
		else
		{
			await sceneB.LoadSceneAsync(loadMode: LoadSceneMode.Additive,
			                            activateOnLoad: true).Task;

			loadBButton.GetComponentInChildren<Text>().text = "Unload Scene B";
		}
	}
	
	public async void LoadC()
	{
		if (sceneC.IsValid())
		{
			await sceneC.UnLoadScene().Task;

			loadCButton.GetComponentInChildren<Text>().text = "Load Scene C";
		}
		else
		{
			await sceneC.LoadSceneAsync(loadMode: LoadSceneMode.Additive,
			                            activateOnLoad: true).Task;

			loadCButton.GetComponentInChildren<Text>().text = "Unload Scene C";
		}
	}

}