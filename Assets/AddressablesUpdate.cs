using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.ResourceLocations;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public static class Formatting
{
	
		
	public const long Kb = 1  * 1024;
	public const long Mb = Kb * 1024;
	public const long Gb = Mb * 1024;
	public const long Tb = Gb * 1024;
	public const long Pb = Tb * 1024;
	public const long Eb = Pb * 1024;



	public static string FormatAsBytes(this long bytes)
	{
		if (bytes < Kb) return $"{bytes:0.##}b";

		if (bytes >= Kb &&
		    bytes < Mb) return $"{(double) bytes / Kb:0.##}Kb";

		if (bytes >= Mb &&
		    bytes < Gb) return $"{(double) bytes / Mb:0.##}Mb";

		if (bytes >= Gb &&
		    bytes < Tb) return $"{(double) bytes / Gb:0.##}Gb";

		if (bytes >= Tb &&
		    bytes < Pb) return $"{(double) bytes / Tb:0.##}Tb";

		if (bytes >= Pb &&
		    bytes < Eb) return $"{(double) bytes / Pb:0.##}Pb";

		if (bytes >= Eb) return $"{(double) bytes / Eb:0.##}Eb";

		return "a whole bunch";
	}

}

public class AddressablesUpdate : MonoBehaviour
{

	public List<string> logs = new List<string>();
	public Text         log;

	public GameObject UI;
	
	public Addressables.MergeMode mergeMode = Addressables.MergeMode.Intersection;
	
	public List<AssetLabelReference> assetLabels = new List<AssetLabelReference>();

	public AssetLabelReference labelA;
	public AssetLabelReference labelB;
	public AssetLabelReference labelC;
	public AssetLabelReference labelD;
	
	public AssetReference sceneA;
	public AssetReference sceneB;
	public AssetReference sceneC;
	public AssetReference sceneD;

	public Button loadAButton;
	public Button loadBButton;
	public Button loadCButton;
	public Button loadDButton;

	private const string LogColor     = "<color=green>● </color>";
	private const string WarningColor = "<color=yellow>● </color>";
	private const string ErrorColor   = "<color=red>● </color>";


	public void Awake() =>
		Application.logMessageReceived += (condition,
		                                   trace,
		                                   type) =>
		                                  {
			                                  condition = type switch
			                                              {
				                                              LogType.Log       => LogColor     + condition,
				                                              LogType.Error     => ErrorColor   + condition,
				                                              LogType.Exception => ErrorColor   + condition,
				                                              LogType.Assert    => WarningColor + condition,
				                                              LogType.Warning   => WarningColor + condition,
				                                              _ => throw new ArgumentOutOfRangeException(paramName: nameof(type),
				                                                                                         actualValue: type,
				                                                                                         message: null)
			                                              };

			                                  Log(msg: condition);
		                                  };


	public async void Start()
	{
		UI.SetActive(false);

		while (Caching.ready == false)
		{
			await Task.Yield();
		}
		
		UI.SetActive(true);
	}

	public void SelectDefaultCache() => Caching.currentCacheForWriting = Caching.GetCacheAt(0);
	public void ClearAllCaches()     => Caching.ClearCache();


	public void CreateCache(string path)
	{
		var preExistingCaches = new List<string>();
		
		Caching.GetAllCachePaths(preExistingCaches);

		if (preExistingCaches.Contains(path))
		{
			Debug.LogError($"Cache [{path}] already exists.");
			
			return;
		}
		
		var c = Caching.AddCache(path);

		Debug.Log(c.valid ? $"Cache [{path}] successfully created" : $"Cache [{path}] creation failed");
	}

	public void CreateCacheA() => CreateCache("A");
	public void CreateCacheB() => CreateCache("B");
	public void CreateCacheC() => CreateCache("C");
	public void CreateCacheD() => CreateCache("D");

	// the default cache is always at 0 and can't be removed, so count Caching.cacheCount from 1 instead of 0!
	// while (Caching.cacheCount < 1 + biomes.Length)
	public void CreateCacheAll()
	{
		CreateCacheA();
		CreateCacheB();
		CreateCacheC();
		CreateCacheD();
	}


	public void SelectCache(string path)
	{
		var outgoing = Caching.defaultCache.path;

		var c = Caching.GetCacheByPath(path); // This will spit its own error if it doesn't exist
		
		Caching.currentCacheForWriting = c;
		
		Debug.Log($"Current Cache changed from [{outgoing}] to [{Caching.currentCacheForWriting.path}]");
	}
	
	public void SelectCacheA() => SelectCache("A");
	public void SelectCacheB() => SelectCache("B");
	public void SelectCacheC() => SelectCache("C");
	public void SelectCacheD() => SelectCache("D");


	public void RemoveCache(string path)
	{
		var c = Caching.GetCacheByPath(path);
		
		var s = Caching.RemoveCache(c);
			
		if (s)
		{
			Debug.Log($"Cache [{path}] successfully removed");
		}
		else
		{
			Debug.LogError($"Cache [{path}] removal failed.");
		}
	}
	
	public void RemoveCacheA() => RemoveCache("A");
	public void RemoveCacheB() => RemoveCache("B");
	public void RemoveCacheC() => RemoveCache("C");
	public void RemoveCacheD() => RemoveCache("D");

	public void RemoveCacheAll()
	{
		RemoveCacheA();
		RemoveCacheB();
		RemoveCacheC();
		RemoveCacheD();
	}


	public void LogCacheSize(string path)
	{
		var c = Caching.GetCacheByPath(path);

		Debug.Log($"Cache [{path}] size [{c.spaceOccupied.FormatAsBytes()}]");
	}
	
	public void LogCacheASize() => LogCacheSize("A");
	public void LogCacheBSize() => LogCacheSize("B");
	public void LogCacheCSize() => LogCacheSize("C");
	public void LogCacheDSize() => LogCacheSize("D");


	public void LogCacheMaxSize(string path)
	{
		var c = Caching.GetCacheByPath(path);

		Debug.Log($"Cache [{path}] max size [{c.maximumAvailableStorageSpace.FormatAsBytes()}]");
	}
	
	public void LogCacheAMaximumSize() => LogCacheMaxSize("A");
	public void LogCacheBMaximumSize() => LogCacheMaxSize("B");
	public void LogCacheCMaximumSize() => LogCacheMaxSize("C");
	public void LogCacheDMaximumSize() => LogCacheMaxSize("D");

	public void LogDownloadSizeA() => LogDownloadSize(labelA);
	public void LogDownloadSizeB() => LogDownloadSize(labelB);
	public void LogDownloadSizeC() => LogDownloadSize(labelC);
	public void LogDownloadSizeD() => LogDownloadSize(labelD);
	public void LogDownloadSizeAll() => LogDownloadSize(null);

	public void LogChangedAssetsA() => LogDownloadSize(labelA);
	public void LogChangedAssetsB() => LogDownloadSize(labelB);
	public void LogChangedAssetsC() => LogDownloadSize(labelC);
	public void LogChangedAssetsD() => LogDownloadSize(labelD);
	public void LogChangedAssetsAll() => LogDownloadSize(null);


	public void DownloadA()
	{
		CreateCacheA();
		SelectCacheA();
		Download(labelA);
	}


	public void DownloadB()
	{
		CreateCacheB();
		SelectCacheB();
		Download(labelB);
	}


	public void DownloadC()
	{
		CreateCacheC();
		SelectCacheC();
		Download(labelC);
	}


	public void DownloadD()
	{
		CreateCacheD();
		SelectCacheD();
		Download(labelD);
	}


	public void DownloadAll() => Download(null);
	
//	public async void CheckForCatalogUpdates()
//	{
//		Debug.Log("This function is broken, so it has been disabled.");
//		
//		return;
//
//		var updatableCatalogs = await Addressables.CheckForCatalogUpdates().Task;
//		
//		Debug.Log("The following Addressables catalogs have updates:");
//
//		if (updatableCatalogs       == null ||
//		    updatableCatalogs.Count == 0)
//		{
//			Debug.Log( "Sweet nothing");
//
//			return;
//		}
//
//		foreach (var c in updatableCatalogs)
//		{
//			Debug.Log( string.IsNullOrEmpty(c) ? "Catalog name unknown" : c);
//		}
//	}


//	public async void UpdateCatalogs()
//	{
//		Debug.Log("This function is broken, so it has been disabled.");
//
//		return;
//
//		var updatedCatalogs = await Addressables.UpdateCatalogs().Task;
//
//		Debug.Log( "The following Addressables catalogs have been updated:");
//
//		if (updatedCatalogs       == null ||
//		    updatedCatalogs.Count == 0)
//		{
//			Debug.Log( "Sweet nothing");
//
//			return;
//		}
//
//		foreach (var c in updatedCatalogs)
//		{
//			Debug.Log( c == null || string.IsNullOrEmpty(c.LocatorId) ? "Catalog name unknown" : c.LocatorId);
//		}
//	}




	public async void LogDownloadSize(AssetLabelReference subGroup)
	{
		var locations = await GetLocations(subGroup);
		
		var downloadTotal = await Addressables.GetDownloadSizeAsync(key: locations).Task;
		
		Log($"Update required [{downloadTotal.FormatAsBytes()}]");
	}


	public async void LogChangedAssets(AssetLabelReference subGroup)
	{
		Log("Which assets have updates?");

		var locations = await GetLocations(subGroup);
		
		foreach (var a in locations)
		{
			var aSize = await Addressables.GetDownloadSizeAsync(key: a).Task; // This is always correct (i.e. 4.34mb)

//			if (aSize <= 0) continue;

			Log($"{a.ResourceType.Name}\t\t[{a.PrimaryKey}]\t\t[{a.HasDependencies}]\t\t[{aSize.FormatAsBytes()}]");

			if (!a.HasDependencies) continue; // HasDependencies returns true as expected

			foreach (var d in a.Dependencies)
			{
				var dSize = await Addressables.GetDownloadSizeAsync(key: d).Task; // This is always 0?

//				if (dSize > 0)
//				{
					// So this never happens...
					Log($"{d.ResourceType.Name}\t\t[{d.PrimaryKey}]\t\t[{a.HasDependencies}]\t\t[{dSize.FormatAsBytes()}]");
//				}
			}
		}
	}


	public async void Download(AssetLabelReference subGroup)
	{
		Debug.Log( "Download requested");

		var locations = await GetLocations(subGroup);

		var downloadOp = Addressables.DownloadDependenciesAsync(locations);

		Debug.Log( "0%");
			
		while (!downloadOp.IsDone)
		{
			await Task.Yield();

			logs.RemoveAt(index: logs.Count - 1);

			Debug.Log( downloadOp.GetDownloadStatus().Percent * 100.0f +"%");
		}

		Debug.Log( "Download complete");
	}
	
	public async Task<IList<IResourceLocation>> GetLocations(AssetLabelReference subGroup)
	{
		while (assetLabels.Count > 1)
		{
			assetLabels.RemoveAt(assetLabels.Count-1);
		}

		if (subGroup != null)
		{
			assetLabels.Add(subGroup);
		}

		var resourceOp = Addressables.LoadResourceLocationsAsync(keys: assetLabels,
		                                                         mode: mergeMode);
		
		var locations = await resourceOp.Task;

		Debug.Log( $"Asset locations found [{locations.Count}]");

		return locations;
	}


	void Log(string msg)
	{
		logs.Add(msg);

		while (logs.Count > 28)
		{
			logs.RemoveAt(0);
		}

		log.text = logs.Aggregate(seed: string.Empty,
		                          func: (current,
		                                 l) => current + "\n" +  l);
	}

	public async void LoadA()
	{
		if (sceneA.IsValid())
		{
			await sceneA.UnLoadScene().Task;

			Log("Scene A unloaded");

			loadAButton.GetComponentInChildren<Text>().text = "Load Scene A";
		}
		else
		{
			await sceneA.LoadSceneAsync(loadMode: LoadSceneMode.Additive,
			                            activateOnLoad: true).Task;

			Log("Scene A loaded");

			loadAButton.GetComponentInChildren<Text>().text = "Unload Scene A";
		}
	}
	
	public async void LoadB()
	{
		if (sceneB.IsValid())
		{
			await sceneB.UnLoadScene().Task;

			Log("Scene B unloaded");

			loadBButton.GetComponentInChildren<Text>().text = "Load Scene B";
		}
		else
		{
			await sceneB.LoadSceneAsync(loadMode: LoadSceneMode.Additive,
			                            activateOnLoad: true).Task;

			Log("Scene B loaded");

			loadBButton.GetComponentInChildren<Text>().text = "Unload Scene B";
		}
	}
	
	public async void LoadC()
	{
		if (sceneC.IsValid())
		{
			await sceneC.UnLoadScene().Task;

			Log("Scene C unloaded");

			loadCButton.GetComponentInChildren<Text>().text = "Load Scene C";
		}
		else
		{
			await sceneC.LoadSceneAsync(loadMode: LoadSceneMode.Additive,
			                            activateOnLoad: true).Task;

			Log("Scene C loaded");

			loadCButton.GetComponentInChildren<Text>().text = "Unload Scene C";
		}
	}
	
	public async void LoadD()
	{
		if (sceneD.IsValid())
		{
			await sceneD.UnLoadScene().Task;

			Log("Scene D unloaded");

			loadDButton.GetComponentInChildren<Text>().text = "Load Scene D";
		}
		else
		{
			await sceneD.LoadSceneAsync(loadMode: LoadSceneMode.Additive,
			                            activateOnLoad: true).Task;

			Log("Scene D loaded");

			loadDButton.GetComponentInChildren<Text>().text = "Unload Scene D";
		}
	}

}